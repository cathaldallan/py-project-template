=======================
Python project template
=======================

A simple python project template to get you started.

Useful links
------------

Google style guide:

* https://github.com/google/styleguide/blob/gh-pages/pyguide.md
* http://sphinxcontrib-napoleon.readthedocs.io/en/latest/example_google.html

Tool for creating isolated Python environments:

* virtualenv: https://virtualenv.pypa.io/en/stable/
* pyenv/pipenv:

    * https://hackernoon.com/reaching-python-development-nirvana-bb5692adf30c
    * https://pipenv.readthedocs.io/en/latest/advanced/#specifying-package-indexes
    * https://pipenv.readthedocs.io/en/latest/basics/#example-pipenv-workflow
    * https://github.com/pyenv/pyenv/wiki/common-build-problems

Tool for dependency management:

* https://pipenv.readthedocs.io/en/latest/


Tool for error detection and coding standard enforcement:

* https://www.pylint.org/

Tool for CI/CD:

* https://docs.gitlab.com/ee/ci/
* https://travis-ci.org

Tool for testing:

* https://docs.pytest.org
* https://tox.readthedocs.io/en/latest/

Database ORM:

* http://docs.sqlalchemy.org/en/latest/orm/tutorial.html


Tools for documentation:

* http://www.sphinx-doc.org/
* http://www.mkdocs.org/

* https://shields.io/

Tools for creating your own or pypi caching server:

* https://pypi.python.org/pypi/devpi-server/
* https://pypi.python.org/pypi/devpi-client/
* https://pypi.python.org/pypi/devpi-web/

Publish packages:

* https://pypi.python.org/pypi/twine

Package versioning convention:

* https://www.python.org/dev/peps/pep-0440/

Ways of single-sourcing a python package:

* https://packaging.python.org/guides/single-sourcing-package-version/



