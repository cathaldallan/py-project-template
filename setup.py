# -*- coding: utf-8 -*-
import io
import os
import sys

from setuptools import setup

PACKAGE_NAME = 'package_name'

with io.open('.version', 'r', encoding='utf-8') as f:
    VERSION = f.read()

if sys.argv[-1] == 'publish':
    os.system('python setup.py sdist upload')
    os.system('python setup.py bdist_wheel upload')
    sys.exit()

if sys.argv[-1] == 'tag':
    os.system("git tag -a {0} -m 'v{0}'".format(VERSION))
    os.system("git push --tags")
    sys.exit()

with io.open('README.rst', 'r', encoding='utf-8') as f:
    README = f.read()

REQUIREMENTS = []

# https://pypi.python.org/pypi?%3Aaction=list_classifiers
CLASSIFIERS = [
    'Programming Language :: Python :: 2',
    'Programming Language :: Python :: 3',
]

KEYWORDS = ''

setup(
    name=PACKAGE_NAME,
    version=VERSION,
    description='',
    long_description=README,
    author='Your name',
    author_email='you.name@example.com',
    url='',
    package_dir={PACKAGE_NAME: PACKAGE_NAME},
    # entry_points={
    #     'console_scripts': [
    #         '{0} = {0}.__main__:main'.format(PACKAGE_NAME),
    #     ]
    # },
    include_package_data=True,
    install_requires=REQUIREMENTS,
    license='',
    zip_safe=False,
    classifiers=CLASSIFIERS,
    keywords=KEYWORDS,
)
